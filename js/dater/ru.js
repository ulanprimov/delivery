function getLastNumber(number){
    if (number<10) return(number);
    number=number+'';
    return(number.slice(-1));
}

function getPreLastNumber(number){
    if (number<10) return(0);
    number=number+'';
    return(number.slice(-2,-1));
}

function showSecond(number){

    /*<10*/
    if (number==1) return('секундa');
    if (number>1 && number<5) return('секунды');
    if (number>4 && number<10) return('секунд');

    /*xx10-xx19*/
    var pre_number=getPreLastNumber(number);
    if (pre_number==1) return('секунд');

    var last_number=getLastNumber(number);
    if (last_number==1) return('секундa');
    if (last_number>1 && last_number<5) return('секунды');

    return('секунд');
}

function showMinute(number){

    /*<10*/
    if (number==1) return('минута');
    if (number>1 && number<5) return('минуты');
    if (number>4 && number<10) return('минут');

    /*xx10-xx19*/
    var pre_number=getPreLastNumber(number);
    if (pre_number==1) return('минут');

    var last_number=getLastNumber(number);
    if (last_number==1) return('минута');
    if (last_number>1 && last_number<5) return('минуты');

    return('минут');
}

function showHour(number){

    /*<10*/
    if (number==1) return('час');
    if (number>1 && number<5) return('часа');
    if (number>4 && number<10) return('часов');

    /*xx10-xx19*/
    var pre_number=getPreLastNumber(number);
    if (pre_number==1) return('часов');

    var last_number=getLastNumber(number);
    if (last_number==1) return('час');
    if (last_number>1 && last_number<5) return('часа');
    return('часов');
}

function showDay(number){

    /*<10*/
    if (number==1) return('день');
    if (number>1 && number<5) return('дня');
    if (number>4 && number<10) return('дней');

    /*xx10-xx19*/
    var pre_number=getPreLastNumber(number);
    if (pre_number==1) return('дней');

    var last_number=getLastNumber(number);
    if (last_number==1) return('день');
    if (last_number>1 && last_number<5) return('дня');
    return('дней');
}

function showMonth(number){

    /*<10*/
    if (number==1) return('месяц');
    if (number>1 && number<5) return('месяца');
    if (number>4 && number<10) return('месяцев');

    /*xx10-xx19*/
    var pre_number=getPreLastNumber(number);
    if (pre_number==1) return('месяцев');

    var last_number=getLastNumber(number);
    if (last_number==1) return('месяц');
    if (last_number>1 && last_number<5) return('месяца');
    return('месяцев');
}

function showYear(number){

    /*<10*/
    if (number==1) return('год');
    if (number>1 && number<5) return('года');
    if (number>4 && number<10) return('лет');

    /*xx10-xx19*/
    var pre_number=getPreLastNumber(number);
    if (pre_number==1) return('лет');

    var last_number=getLastNumber(number);
    if (last_number==1) return('год');
    if (last_number>1 && last_number<5) return('года');
    return('лет');
}