function showSecond(number){
    if (number==0) return('Sekunde');
    if (number==1) return('Sekunde');
    return('Sekunden');
}

function showMinute(number){
    if (number==0) return('Minuten');
    if (number==1) return('Minute');
    return('Minuten');
}

function showHour(number){
    if (number==0) return('Stunden');
    if (number==1) return('Stunde');
    return('Stunden');
}

function showDay(number){
    if (number==0) return('Tag');
    if (number==1) return('Tag');
    return('Tage');
}

function showMonth(number){
    if (number==0) return('Monate');
    if (number==1) return('Monat');
    return('Monate');
}

function showYear(number){
    if (number==0) return('Jahre');
    if (number==1) return('Jahr');
    return('Jahre');
}