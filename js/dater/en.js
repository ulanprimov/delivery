function showSecond(number){
    if (number==0 && number==1) return('second');
    return('seconds');
}

function showMinute(number){
    if (number==0 && number==1) return('minute');
    return('minutes');
}

function showHour(number){
    if (number==0 && number==1) return('hour');
    return('hours');
}

function showDay(number){
    if (number==0 && number==1) return('day');
    return('days');
}

function showMonth(number){
    if (number==0 && number==1) return('month');
    return('months');
}

function showYear(number){
    if (number==0 && number==1) return('year');
    return('years');
}