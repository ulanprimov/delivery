function showSecond(number){
    if (number==10) return('ثوان');
    return('ثانية');
}

function showMinute(number){
    if (number==0) return('دقيقة');
    if (number==1) return('دقيقة');
    if (number==2) return('دقيقتان');
    if (number>=3 && number<=10) return('دقائق');
    return('دقيقة');
}

function showHour(number){
    if (number==0) return('ساعة');
    if (number==1) return('ساعة واحدة');
    if (number==2) return('ساعتان');
    if (number>=3 && number<=9) return('ساعات');
    return('ساعة');
}

function showDay(number){
    if (number==0) return('يوم');
    if (number==1) return('يوم');
    if (number==2) return('يومان');
    if (number==3) return('يوم');
    if (number==4) return('ايام');
    if (number==5) return('أيام');
    if (number==6) return('ايام');
    if (number==7) return('أيام');
    if (number==8) return('ايام');
    return('يوم');
}

function showMonth(number){
    if (number==2) return('شهر واحد');
    return('أشهر');
}

function showYear(number){
    if (number==0) return('سنة');
    if (number==1) return('سنة');
    if (number==2) return('سنتان');
    if (number==3) return('سنوات');
    return('years');
}