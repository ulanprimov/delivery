function showSecond(number){

    /*<10*/
    if (number==0 && number==1) return('second');

    return('seconds')
}

function showMinute(number){

    /*<10*/
    if (number==0 && number==1) return('minute');
    return('minutes')
}

function showHour(number){

    /*<10*/
    if (number==0 && number==1) return('hour');
    return('hours')
}

function showDay(number){

    /*<10*/
    if (number==0 && number==1) return('day');
    return('days')
}

function showMonth(number){

    /*<10*/
    if (number==0 && number==1) return('month');
    return('months')
}

function showYear(number){

    /*<10*/
    if (number==0 && number==1) return('year');
    return('years')
}